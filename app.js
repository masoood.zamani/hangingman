let _letters = ['DOG', 'BOOK', 'BAG', 'SHOP', 'APPLE', 'HOUSE', 'LIBRARY', 'OFFICE']

let _selected = [];

let _invalid = [];

let btnList = [];

let timerEl;

let fullTime = 10 * 60 * 1000;

let _randomLetter = '';

function initKeyboard() {
    let keyborad = document.getElementById("keyborad");
    keyborad.innerHTML = '';

    for (let index = 'A'.charCodeAt(0); index <= 'Z'.charCodeAt(0); index++) {
        let btn = document.createElement("button");

        let chr = String.fromCharCode(index);;

        btn.classList = "btn btn-default"
        btn.innerText = chr;
        btn.value = chr;

        btn.onclick = () => {

            if (btn.disabled || _randomLetter == "") { return; }

            let idx = _randomLetter.indexOf(chr);
            if (idx != -1) {
                let guess = document.getElementById("correctLetter").value;
                guess = replaceIndex(guess, idx, chr);
                _randomLetter = replaceIndex(_randomLetter, idx, "_")
                document.getElementById("correctLetter").value = guess;
                if (guess.indexOf("_")  == -1) {
                    alert("ok you win");
                }
            }
            else if (_invalid.length < 10) {
                _invalid.push(chr);
                document.getElementById("wrongletter").value = _invalid.join("");btn.disabled = true;
            }
            else {
                alert("game over");
            }

            document.getElementById("img").src = `images/hm${_invalid.length + 1}.gif`

            //


        }
        btnList.push(btn);
        keyborad.appendChild(btn);
    }
}

function replaceIndex(input, index, replacement) {
    return input.substr(0, index) + replacement + input.substr(index + replacement.length);
}

let intervallRef;

function start() {
    btnList.forEach(b => b.disabled = false);
    let idx = parseInt(Math.random() * 100000) % _letters.length;
    _randomLetter = _letters[idx];
    console.log(_randomLetter);
    let rpc = _randomLetter.split("").map(a => "_").join("")
    document.getElementById("correctLetter").value = rpc;

    clearInterval(intervallRef);

    fullTime = 10 * 60 * 1000;

    _invalid = [];
    _selected = [];


    intervallRef = setInterval(() => {
        fullTime -= 1000;
        timerEl.innerText = parseInt(fullTime / 60000) + ":" + fullTime % 60000
    }, 1000);
}

window.onload = () => {
    timerEl = document.getElementById("timer");
    initKeyboard();
}



